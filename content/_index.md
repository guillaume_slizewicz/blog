---
title: 
language: en
slug: /Hugo/
---

<script async src="https://code.nightnight.xn--q9jyb4c/js/2200/0600/nightnight.js"></script>

<iframe src="https://player.vimeo.com/video/787991805?h=c7440f3fbf&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>


Guillaume Slizewicz speaks in the language of technology but tells an emotional story. He translates complex social issues, from local air quality to online surveillance, into objects and installations that combine digital and physical (im)materials. Glass, algorithms, wood, social sciences, ceramics, circuit boards, literature, photography, code, a botanical garden, robots, computer voices, light – in Guillaume’s mind and hands they are all materials to be shaped into engaging experiences for all audiences, from the interested amateur to a demanding specialist. Right now Guillaume focuses on the notion of vernacular electronics, or how to give new technologies a local shape, form, and content.

After graduating in politics, philosophy, and economics in 2012, Guillaume worked as a strategist and consultant for different agencies and clients that were looking to innovate how they relate to their audiences. During these years he gravitated towards design thinking, product design and finally data visualisation and machine learning, leading him to complete a BA in Production Technology at Copenhagen School of Design and Technology (KEA) and an internship with interactive design studio Superbe in Namur.

Landing in Brussels, he started to work as a design researcher for the Urban Species research group by LUCA School of Arts and ULB in Brussels. In parallel he set up his own studio in 2020 to create, produce and exhibit more artistic design projects that reflect on the plural relationships between technology, nature and society. Often working in collectives such as Algolit, Anaïs Berck or Tropozone, Guillaume Slizewicz’s work has been presented by institutions like Design Museum (Ghent), Kikk (Namur) and BioArt Labs (Eindhoven), by universities in Brussels, Basel and Hong Kong, as well as in grassroots venues deep in the local urban fabric such as Biestebroekbis, La Maison du Livre St Gilles and Constant in Brussels. 

Text by Alexander Marinus for MAD Brussels

[Notes](/posts/) &bull; [Categories](categories/) &bull; [Tags](tags/)

[Instagram](https://instagram.com/guillaume_slizewicz) &bull; [twitter](https://twitter.com/Guillaume_Slize)

