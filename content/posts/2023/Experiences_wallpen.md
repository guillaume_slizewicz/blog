---
title: Honest Feedback on a Wallpen Vertical Printer
date: 2024-01-10T10:35:04+02:00
categories:
  - machines
tags:
  - design
  - code
  - Algorithms
  - Vertical Printing
  - Exhibition Design
  - UVink
  - midjourney
  - Photography
language: en
slug: Vertical_Printer

---

![Credit  Jente_Waerzeggers](/img/vertical_printer/printer_credit_Jente_Waerzeggers4.jpg)




The Green Type project, spanning from September to December 2023, was an explorative project into the possibilities of sustainable exhibition design. The focus was on assessing the viability of vertical printing technology, particularly through the use of a UV-vertical Wallpen printer, as a potential alternative to traditional vinyl lettering. Through experiments I came to a few conclusions about the  advantages and drawbacks of such technologies.


### Drawbacks of the Wallpen Printer:
 
 


__Transportation Challenges:__ The Wallpen, robust in build, presents logistical challenges due to its bulk and weight. My initial idea to transport it via a cargo bike proved impractical, as its size and heft demanded more conventional means. Even limited transport experiences, including climbing stairs, highlighted its cumbersome nature. The machine feels very engineer-y, not very user friendly. It is strong, sturdy and stable, at the expense of ease of use. You can feel the love of machining in the steel bars.



__Ink Sustainability Concerns:__  A primary concern for the project's sustainability goals is the Wallpen's ink. When uncured, it emits a significant amount of volatile organic compounds (VOCs), posing health risks and releasing a strong odor for a few hours. The necessity of using a FFP2 mask during operation in small spaces raised questions about long-term health implications.



__UV Light Exposure:__ The Wallpen's ink curing process involves intense UV light, potentially harmful to eyesight. The constant requirement for wearing polycarbonate goggles, especially in conjunction with a FFP2 mask, was cumbersome (you have mist on your goggles all the time). Public demonstrations demanded additional safety measures, particularly when printing on transparent surfaces (use a blocker when printing on glass).



__Operational Rigidity:__ A notable drawback of the Wallpen is its lack of forgiveness for operational errors. Mistakes during printing often necessitated repainting walls, a laborious and time-consuming task. This inflexibility poses significant challenges, especially when working in spaces where repainting isn't feasible. I had to repaint a wall twice in the timespan of the project, I was lucky that I could only repaint part of the wall but I can imagine this being a massive pain when using only lettering and having to paint a full 4x3m wall.



__Color Calibration:__ Achieving the desired visual output required extensive knowledge of color calibration. The machine's default settings often produced overly dark prints, necessitating substantial adjustments in lightness and color balance. This steep learning curve in color management underscores the need for comprehensive pre-testing for each project. (I had to boost the lightness on photoshop to 60% most of the time to get good results. On my machine I also dropped the CYAN and added a bit of yellow.)



__Printer Calibration:__ The Wallpen's calibration process, both vertically and horizontally, is important. Every transportation or accidental touch necessitates recalibration, making the setup time-consuming, typically requiring around 1 hours for assembly, ink refill, and calibration. (The 20 minutes set-up on wallpen website is very optimistic.)


![Credit  Jente_Waerzeggers](/img/vertical_printer/printer_credit_Jente_Waerzeggers1.jpg)


__Software Issues:__ While generally efficient, the software occasionally presents issues, such as artifacts or missing parts in printed images, necessitating vigilance during operation.



__Obstacle Avoidance:__ The printer lacks environmental awareness, posing risks of collisions. A programmed downward movement at startup can lead to impacts if not planned.



__Ink Management:__ Predicting ink levels is challenging. The printer does not provide an indication of remaining ink, requiring regular manual checks and refills.

 

__Spatial Memory:__ The Wallpen does not track its position in space or on its rails, making it unable to resume a partially completed print accurately.



__Longevity Concerns ?:__ Though not personally experienced, discussions in forums indicate that some users have encountered issues with the printer's components, such as the printing heads. However, these can be replaced.


### Proposed Solutions and Reflections:

To address the challenges faced with the Wallpen printer and make it a more viable competitor against vinyl, several steps could be implemented:

- __Develop Eco-friendly Ink:__ Focusing on creating more sustainable ink options would enhance the Wallpen's appeal, particularly in terms of environmental impact. At the moment, it is hard to really know the impact of the used inks. A carbon and sustainability assessement of the current ink could be an alternative.

- __Improved Transportation System:__ Innovating a lighter and more transport-friendly system would make the printer more accessible and practical for diverse settings.

- __Advanced Collision Avoidance Technology:__ Incorporating bump technology, similar to that used in robotic vacuums, would improve the printer's environmental awareness and prevent accidental impacts. 

- __Enhanced Ink Management__: Implementing a more accurate ink monitoring system would provide better predictability and efficiency in ink usage, reducing operational uncertainties.


### Marketing Practices and ROI Challenges:
 
The marketing approach on the Wallpen website emphasizes the return on investment while obscuring the printer's actual cost. Conversations with Wallpen owners revealed a disconnect between the marketing claims and real-world experiences. None of these owners had managed to recoup their investment yet and they reported a lack of traction / a sluggish market, suggesting a potential overstatement of profitability in promotional materials. 
 
### Print Quality Impressions: 

In contrast, the Wallpen printer's quality in actual use was remarkable. The finesse of detail and the depth of the blacks in prints were consistently impressive. This high quality received widespread praise during demonstrations, underscoring the machine's capability to produce visually striking outputs.

### First feedback on this post:

In response to this article, a range of feedback has been received, reflecting diverse experiences and perspectives:

Safety: One reader noted that while the ink emits a strong odor during printing, it becomes non-toxic once cured - a significant advantage over traditional wallpaper or vinyl that can still release toxic compounds. They highlighted that wearing a mask is a minor inconvenience compared to the benefits of the printer. They also pointed out that the need for safety glasses is not a major issue since their work environment minimizes public interaction, reducing the need for extra safety measures.

Technology Acceptance and Realism: Another reader emphasized the importance of accepting the realities of cutting-edge technology. They argued that certain drawbacks, like ink VOCs or operational rigidity, are part and parcel of working with such technology. The reader expressed that pioneers of new technology must accept its imperfections as part of a learning process, rather than expecting flawless functionality from the outset.

Frustration with Technical Issues: A third response revealed a more critical stance, expressing regret in their purchase of the Wallpen due to numerous operational problems, seconded by another user concerning the durability of the ricoh head. Despite acknowledging the innovative nature of the technology, they highlighted the frustration and disappointment stemming from its persistent issues. Two reader explained they had had their wallpen for 6 years with only a few problems. Other expressed their surprise for the short warranty period compared to similar product (one year instead of 2) and their frustration with the price of the spare parts.

 ### Acknowledgements:

 All pictures were made by Jente Waerzeggers during the set-up of the Promptism exhibition.
 
 This post is part of the Greentype project, supported by the #creathriveEU from Basilicatacreativa and the European Commission and  was co-funded by the European Union’s Single Market Programme

  ![Credit  Jente_Waerzeggers](/img/vertical_printer/printer_credit_Jente_Waerzeggers7.jpg)

 
  ![Logo EU](/img/vertical_printer/EN_Co-fundedbytheEU_RGB_BLACK.png)


<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/CxYShOkt5dR/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CxYShOkt5dR/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;">Voir cette publication sur Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CxYShOkt5dR/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Une publication partagée par Guillaume Slizewicz (@guillaume_slizewicz)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

![Credit  Jente_Waerzeggers](/img/vertical_printer/printer_credit_Jente_Waerzeggers11.jpg)


