---
title: Robigami
date: 2018-08-14T10:35:04+02:00
categories: [machines,workshop]
tags: [origami, robots, arduino, electronics, animals, kids]
language: en
slug: robigami
---

![Robigami](/img/Robigami.jpg)

[Robigami](http://www.capitainefutur.voyage/les-ateliers/robigami-kikk) est une série d’ateliers visant à faire découvrir aux enfant  de 10 à 12 ans des techniques simples d’origami ainsi que les bases de  la programmation grâce à la plateforme arduino. Inspiré par l’oeuvre [*Edge of Chaos*](http://capitainefutur.voyage/les-oeuvres/the-edge-of-chaos)  d’Interactive Architecture Lab et de AMOLF/Studio Overvelde et ses  structures changeantes et interactives, Robigami revisite l’art du  papier plié en le combinant à l’électronique. 

Crée tes propres robots origamis et fais-les réagir à la lumière, au  toucher ou à d’autres créatures. Donne vie à un monde différent du nôtre  où les dragons peuvent être peureux, les plantes jouer le rôle du  soleil et où la cellulose et l’électricité remplacent l’eau et la  chlorophylle.

L’atelier a été conçu avec la complicité de  l’équipe du KIKK festival. Un tutoriel a été réalisé et est disponible sur le site de Capitaine Futur afin de reproduire  l’atelier qui se découpe en plusieurs étapes qu’il est possible de  réaliser seules. En tout, Robigami dure 4 heures.

L'atelier a été mené aux à la Gaîté Lyrique à Paris, à Multiplica au Luxembourg, à WoeLab au Togo et au Kikk  festival, au Trakk à Namur.



![robigami2](/img/robigami3.jpg)

![robigami4](/img/robigami4.jpg)

![robigami_rotonde](s/img/robigami_rotonde.jpg)

 *Robigami à [Rotondes](<https://www.facebook.com/RotondesLuxembourg/?__tn__=%2Cd%2CP-R&eid=ARCYugIYe8-qmMHiGV5R0r2i417HflkWEa1eMfa7OJvCYAUlz0Vir1WTr9dWyuSjvwcKgvdVw8u4K0HD>)*