---
title: The Devogram
date: 2018-12-30T17:16:01+02:00
categories: [machines]
tags: [code, robots, electronics,python,raspberry,arduino]
language: en
slug: the_devogram

---



### Description

The devogram is a mobile broadcasting robot,a political amplifier for urban environment. It can track people, follow them and "talk" to them, broadcasting sound or sentences prepared in advance. 

The devogram can detect objects and humans in the street thanks to an object detection algorithm (namely mobilenet v2 with the COCO dataset and labels). In this version, the sentences it reads comes from what an OCR algorithm could decypher from images of a public street, "le pietonnier", in Brussels.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kB1oQwy2_7s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



![animal](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/animal.gif)

![children_3](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/children_3.gif)

### ![shoot](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/shoot.gif)



### Hardware

The devogram is a robot containing one arduino board with a motor shield and four actuators. In its original form it ran with a raspberry pi, a pi camera, one lithium battery and a speaker.
Latest version of the devogram are running on a google coral board, resulting in faster interactions and movements.

![devo_2](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/devo_2.jpg)

![devo_3](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/devo_3.jpg)

![devo_4](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/devo_4.jpg)

![devo_5](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/devo_21.JPG)

![devo_6](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/devo_22.JPG)

![devo_7](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/devo_23.JPG)

![devo_7](https://gitlab.com/guillaume_slizewicz/stray_peddler/raw/master/img/devo_24.JPG)



### Software

The devogram scripts are written in Python and Arduino C++ , you can find them on [the gitlab page](https://gitlab.com/guillaume_slizewicz/stray_peddler)

### Acknowledgment

The project was created with the help of [this tutorial](https://github.com/EdjeElectronics/TensorFlow-Object-Detection-on-the-Raspberry-Pi)

### About Urban Species

We are Urban Species, a research group based in Brussels focusing on citizen participation. We are a multi-disciplinary team at the crossroad of anthropology, urban planning, design and sociology. Urban Species brings together researchers from ULB and Luca School of Arts. As part of its prototyping strategies and experiments, Urban Species collaborates with developers, graphic designers, but also citizens, associations, public authorities and a whole series of species, human or not, including electronic and digital fauna.

Urban Species is currently engaged in two projects, P-lab and SUCIB, which are part of Innoviris' Anticipate program, under the theme "Societal participation and citizenship".

Find more at: https://www.instagram.com/especesurbaines/



