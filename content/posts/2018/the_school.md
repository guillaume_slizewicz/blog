---
title: "The School"
date: 2018-03-22T18:42:48+01:00
categories: [design,workshop]
tags: [community, city, scaffolding, place-making, design, processing, workshops]
language: en
slug: the_school
---

![the_school3](/img/the_school3.jpg)

*Hasselt Courthouse*,
Digital Photography,
2018

In november 2017, I had the chance to join the [The School](https://www.theschool.city/) as a designer in residence where I met [Mia](https://miamia.studio), [Nina](http://www.ninajorgensen.com), [Iris](https://irisvanderheijden.com) among many other talented graphists, designers and artists.

The school is  a place for experimentation in Hasselt, a small town in Flanders, Belgium. 

During 4 months, we organised workshops, festivals and created artworks.



![workshop_1](/img/POmPom-1.jpg )

![workshop_2](/img/workshop.jpg )

![workshop_2](/img/workshop_in_disco.png )

*Workshops in Disco*,
Community building workshops,
2017

![the_school1](/img/the_school1.jpg)

![the_school2](/img/the_school2.jpg)

![the_school4](/img/the_school4.jpg)

*An Altar for Hasselt*,
Bamboo poles, ropes, glass jars and candles,
2017

![the_school5](/img/the_school5.jpg)

![the_school6](/img/the_school6.jpg)

*Natural pigments workshop with [Kristof Vrancken](https://www.instagram.com/kristofvrancken/)*
Red Cabbage and flowers,
2018

