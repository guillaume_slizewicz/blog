---
title: "Grow Paris"
date: 2018-11-30T18:52:20+01:00
categories: [code]
tags: [code,neural networks,python,paperspace,data]
language: en
Slug: grow_paris

---

 ![grow](/img/grow.png)

In november I had the chance the be invited to give a talk at [Grow Paris](https://twitter.com/GROW_Paris) during the New Kids competitions. I presented Latent Landscape from Brittany, a image generator created by feeding a neural network  with postcards from Brittany.

Here is a sample of what it can generate:

<video  controls="controls" src="/img/Grow_Paris.mp4"></video>

<iframe width="560" height="315" src="https://www.youtube.com/embed/JGgYrSJyc7Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

