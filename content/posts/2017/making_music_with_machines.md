---
title: Machine Music Making
date: 2017-12-04T12:17:01+02:00
categories: [machines]
tags: [code, music, electronics]
language: en
slug: making_music_with_machines
---



![maximom_1](/img/maximom_1.jpeg)

This Summer I spent two months at [Superbe](http://superbe.be) working on the Maximom, a playful music box.

I went through all product development phase from defining the specification to drawing, prototyping and producing it. We have now a model for our locally produced music box. It can be made using simple tools (laser cutter and CNC) at any common workshop area.



## Process

![First drawing of the maximom](/img/maximom_sketches.png )*First drawing of the maximom*

![Second prototype in Acrylic and MDF](/img/maximom_2.png)*Second prototype in Acrylic and MDF*

![Assembled PCB](/img/maximom_5.png)*Assembled PCB*

![](/img/maximom_4.png)

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/5dvfbSgO1Wo" frameborder="0" allowfullscreen></iframe></center>

![The full serie](/img/maximom_6.png)*The full serie*
