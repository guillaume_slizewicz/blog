---
title: Creating a new identity for Biologigaragen
date: 2017-06-04T12:17:01+02:00
categories: [design]
tags: [graphic design, citizen science, science]
language: en
slug: biologigaragen

---



![the new logo](/img/biologigaragen8.png)

During my stay at Copenhagen, I got involved in Biologaragen, a citizen science project promoting DIY science and experiments. Among other things, we organised a bio-plastic workshop, and a “wear your own DNA” workshop. Plus we were present at different events such as maker Faire Copenhagen, Klick, and Bloom

![Natural colors and Microscope at Bloom](/img/biologigaragen7.jpeg)

*Microscope and natural pigments at Bloom*

2017

We soon realised that people did not really understand who we were, and we were struggling to get traction, as people did not realise that we were an independent entity and associated us with the different events we took part in. We thought that there was an opportunity in changing our identity and communication channels, to make us look more professional and active.

![Nordic Tofu Workshop at Bloom](/img/biologigaragen6.jpg)

*Nordic Tofu Workshop at Bloom*

2017

Before leaving for Brussels, I created a new identity to the association.

I think one of the challenges of working for associations and volunteer-run associations is dealing with the sense of ownership that people develop. I tried to take it into account in my presentation.

Here is the deck I used.
[**Biologigaragen Brand ID**
*Biologigaragen Identity proposal](https://docs.google.com/presentation/d/1ehLJMK24ODKeECxU5Hd3CeK95NJjGIXMZPXbLxMXFbg/edit?usp=sharing)*

Some selected slides:

![Some ideas that led to the creation of the logo.](/img/biologigaragen5.png)

![](/img/biologigaragen4.jpeg)

![Example for event promotion](/img/biologigaragen3.jpeg)

![](/img/biologigaragen2.jpeg)

![](/img/biologigaragen1.jpeg)

More about DIY bio in Copenhagen on their [facebook](https://www.facebook.com/Biologigaragen/) or [website](http://biologigaragen.org/).
