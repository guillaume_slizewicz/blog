---
title: Creating Future visions through an old medium
date: 2021-01-15T10:35:04+02:00
categories: [code]
tags: [design, code, algorithms, Machine Learning, Public Space, Speculations, Retrofutures, Photography, colab]
language: en
slug: Future_vision

---



At [urban species](http://www.urbanspecies.org), we often merge observation of real life, situated place with speculative visions of the future to create installations that can trigger curiosity and allow us to start a conversation with people in public space. 

In this post, I would like to go back to an experiment/ intervention we did last year. We created custom and bespoke view master reels to present alternative visions of the future in the framework of a partnership with perspective, an institution based in Brussels responsible for urban planning. 

In this experiment, we used machine learning technologies to turn our 2D images into stereo-images. 

Stereophotography is relatively old technology, and I was first introduced to it when I discovered the old stereo pictures my great-great-father took during the first world war similar to the one below.

![war](/img/bordet/war.jpg)

You can also see it in roaring twenties series such as Babylon berlin. It kept being typical tourist memorabilia and you still often find view master reels of the royal wedding of King Baudouin at the flea market in Brussels. 

![royal_wedding](/img/bordet/royal_wedding.jpg)



#The code

To make our ones, we went through the steps of analysing the depth of 2D images thanks to [the algorithm developed by Meng-Li Shih, Shih-Yang Su, Johannes Kopf,  Jia-Bin Huang](https://shihmengli.github.io/3D-Photo-Inpainting/) which gave us an image showing the different depth of the image (as recognised by the algorithm) in different shade of grey. We then ran it through [depth2stereo](https://github.com/marchese29/depth2stereo), an algorithm that duplicate your image and add pixel according to the depth map you gave it, resulting in a double image, side by side, with tiny differences.



This is what it gave with one of our photos:

![small_glamping3](/img/bordet/small_glamping3.jpg)

![depth_small_glamping3](/img/bordet/depth_small_glamping3.png)



![stereo_small_glamping3](/img/bordet/stereo_small_glamping3.jpg)



And the result with a poster, without real 3d depth and which, in my opinion, gave the best results:

![glamping_6](/img/bordet/glamping_6.jpg)

![depth_small_glamping_6](/img/bordet/depth_small_glamping_6.png)

![stereo_small_glamping_6](/img/bordet/stereo_small_glamping_6.jpg)



The funny thing is that you can also see 3d images by crossing your eyes (be careful of headaches), and it is explained [here](http://www.neilcreek.com/2008/02/28/how-to-see-3d-photos/).

You can also then run your images through an image enhancement algorithm such as the one proposed [here](https://colab.research.google.com/github/tugstugi/dl-colab-notebooks/blob/master/notebooks/ISR_Prediction_Tutorial.ipynb#scrollTo=3qlX0cBCtESJ&uniqifier=2) from [here](https://github.com/idealo/image-super-resolution)

Finding a company to print the reels was complicated but we found a company called digital slides based in the UK that did a fine job and even sent them in a retro sleeve. Another option is to go full DIY and follow this [instructable](https://www.instructables.com/DIY-Viewmaster-Reels/)
![glamping_6](/img/bordet/retro.jpg)

We then used them with our old, bakelite view master, providing a way to see the future through an old lens

![Proto4-7](/img/bordet/Proto4-7.jpg)