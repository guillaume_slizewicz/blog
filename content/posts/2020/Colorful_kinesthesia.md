---
title: Colourful Kinesthesia
date: 2020-03-29T10:35:04+02:00
categories: [design]
tags: [design, Outfits, Choreography, Directing, Movie, Boisbuchet, Pink, Colour,Kinesthesia]
language: en
slug: colourful_kinesthesia
---





<iframe src="https://player.vimeo.com/video/437053609" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/437053609">Edizalp Akin and Guillaume Slizewicz - Pink - Performance at domaine de Boisbuchet 2019</a> from <a href="https://vimeo.com/user116939240">Guillaume Slizewicz</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
Colourful Kinesthesia is the result of the Workshop led by MischerTraxler Studio at Boisbuchet in 2019. It is a playful experiment on the meaning of colour pink and how it could be translated into movements. 
We started by defining words that relate to pink such as cheeky, naive, soft, artificial and fantasy.
Then we attempted to translate those words into physical gestures and asked ourselves what movements could be considered as naive or cheeky, artificial or whimsical. We settled on hopping, bouncing, and running amok.
The result of these experiments is the video above. The photography is by Holo Wang. It was directed, choreographed, and the outfits were designed by Edizalp Akin and Myself.

Images of the process below by Martina Orska

![colorful_kinestesia_1](/img/colourful_kinesthesia/colorful_kinestesia_1.jpg)

![colorful_kinestesia_2](/img/colourful_kinesthesia/colorful_kinestesia_2.jpg)

![colorful_kinestesia_3](/img/colourful_kinesthesia/colorful_kinestesia_3.jpg)



Kinaesthesia: noun, awareness of the position and movement of the parts of the body by means of sensory organs in the muscles and joints | from Greek kinein ‘to move’ + aisthēsis ‘sensation’.