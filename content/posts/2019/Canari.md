---
title: Canari 
date: 2019-09-17T18:56:04+02:00
categories: [code]
tags: [urban species, artworks, lufdaten, air quality, hackair, aircasting, light,  scrapping]
language: en
slug: Canari
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/PjhpzW2GObY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Canari is an artwork that transform local air quality data into light patterns. It draws its inspiration from the canari, a bird that was the coal-miners companion and alerted them when the air was contaminated.
The piece is made of salvaged brass sockets and glass lampshades mounted on a black steel structure. The data is scrapped from the internet in real time via a raspberry pi.



 ![Canari_extended (101 of 16)](/img/canari/Canari_extended1.jpg)

![Canari_extended (104 of 16)](/img/canari/Canari_extended2.jpg)

![Canari_extended (106 of 16)](/img/canari/Canari_extended3.jpg)

![Canari_extended (109 of 16)](/img/canari/Canari_extended4.jpg)

![Canari_extended (115 of 16)](/img/canari/Canari_extended5.jpg)