---
title: Algorithmic readings of Bertillon's portrait parlé
date: 2019-04-14T12:35:04+02:00
categories: [code]
tags: [algolit, artworks, p5js, documentation, constant, bertillonage, face recognition, dataworkers, Otlet]
language: en
slug: Algorithmic_readings
---

![data_worker1](/img/data_worker1.jpg)

*Algorithmic readings of Bertillon's portrait parlé* was conceived as part of [Data Workers](https://www.algolit.net/index.php/Data_Workers), an exhibition by algolit in Mons' Mundaneum. 

Written in 1907, Un code télégraphique du portrait parlé is an attempt to translate the 'spoken portrait'(a face-description technique created by Alphonse Bertillon, the inventor of legal antropometry in Paris) into numbers. By implementing this code, it was hoped that faces of criminals and fugitives could easily be communicated over the telegraphic network in between countries. In its form, content and ambition this text represents our complicated relationship with documentation technologies.

![data_worker2](/img/data_worker2.jpg)

![data_worker6](/img/data_worker6.jpg)

![data_worker7](/img/data_worker7.jpg)



*Writing with Otlet,*
Mixed Media,
2019

Writing with Otlet is a character generator that uses the spoken portrait code as its database. Random numbers are generated and translated into a set of features. By creating unique instances, the algorithm reveals the richness of the description that is possible with the portrait code while at the same time embodying its nuances. 

*An interpretation of Bertillon's spoken portrait*,
Mixed media,
2019

This work draws a parallel between Bertillon systems and current ones. A webcam linked to a facial recognition algorithm captures the beholder's face and translates it into numbers on a canvas, displaying it alongside Bertillon's labelled faces. 

Read more on [algolit.net](https://www.algolit.net)



![mundaneum_Javi](/img/mundaneum_javi_2.jpg)

*Photo by Javier Lloret*



![mundaneum_Javi](/img/mundaneum_Javi.jpg)

*Photo by Javier Lloret*