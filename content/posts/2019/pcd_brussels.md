---
title: Processing Community Day Brussels
date: 2019-02-06T12:35:04+02:00
categories: [code,workshop]
tags: [community, PCD, Processing, p5js]
language: en
slug: pcd_brussels
---

![pcdbyteweek](/img/pcdbyteweek.jpg)

Processing is a langage for creating visual art with code. This February  [Martin Pirson](https://www.linkedin.com/in/martinpirson/) and I organised a working session to gather the community in Brussels during which [Frederik Vanhoutte](https://www.wblut.com/) accepted to come to present some of his work. We were kindly hosted by [Hackerspace Brussels](https://hsbxl.be/) .



Here are some of the ressources and exercises we played around with. 





#  Processing Community Day Brussels 



Editor: [P5.js online editor](https://editor.p5js.org/) - please make an account

Offical ressources: [P5.js website](https://p5js.org/) / [P5.js reference](https://p5js.org/reference/) / [P5.js examples](https://p5js.org/examples/)

Artistic ressources: [Generative Design P5.js](http://www.generative-gestaltung.de/2/) / [OpenProcessing](https://www.openprocessing.org/)

Tutorials: [Coordinate System and Shapes](https://p5js.org/learn/coordinate-system-and-shapes.html) & [Interactivity](https://p5js.org/learn/interactivity.html) / [P5.js cheat sheet pdf](https://andreasref.github.io/malmedkodning/cheatsheet.pdf) 

Most Sketches ressources based on Andreas Refsgaard course on Machine Learning for Designers
[andreasrefsgaard.dk](http://andreasrefsgaard.dk) ——> Machine Learning and P5.js



### Basics

[1_1_Basic_Shapes](https://editor.p5js.org/guillaume_slizewicz/sketches/3d78bLxLQ)

<iframe src="https://alpha.editor.p5js.org/embed/ryrW4i7NM" width="400px" height="400px"></iframe>

#### *Exercise: Draw a face using the simple in 1_1_Basic_Shapes. Remember to save it!*

[1_2_Basic_Shapes_colors](https://editor.p5js.org/guillaume_slizewicz/sketches/3d78bLxLQ)

<iframe src="https://alpha.editor.p5js.org/embed/3d78bLxLQ" width="400px" height="400px"></iframe>

*Exercise: Color your face using the 1_2_Basic_Shapes_colors. Remember to save it!*



[Example](https://editor.p5js.org/guillaume_slizewicz/sketches/da6zBmB1O)

<iframe src="https://alpha.editor.p5js.org/embed/da6zBmB1O" width="400px" height="400px"></iframe>

### Interactivity

[3_1_Interactivity_Mouse (press mouse to draw)](http://alpha.editor.p5js.org/AndreasRef/sketches/H1bwK4HEf)

<iframe src="https://alpha.editor.p5js.org/embed/H1bwK4HEf" width="400px" height="400px"></iframe>

[3_2_Interactivity_Keys (press 'r', 'g' or 'b')](http://alpha.editor.p5js.org/AndreasRef/sketches/r1_v6NH4f)

<iframe src="https://alpha.editor.p5js.org/embed/r1_v6NH4f" width="400px" height="400px"></iframe>

### Math

[4_1_Math_Random](http://alpha.editor.p5js.org/AndreasRef/sketches/HkdG0EB4f)

<iframe src="https://alpha.editor.p5js.org/embed/HkdG0EB4f" width="400px" height="400px"></iframe>

[4_2_Math_Noise](http://alpha.editor.p5js.org/AndreasRef/sketches/BkzBCNSEG)

<iframe src="https://alpha.editor.p5js.org/embed/BkzBCNSEG" width="400px" height="400px"></iframe>

[4_3_Math_Sin](http://alpha.editor.p5js.org/guillaume_slizewicz/sketches/YL1cy1CyG)

<iframe src="https://alpha.editor.p5js.org/embed/YL1cy1CyG" width="400px" height="400px"></iframe>

[4_4_Math_Map (controlled by mouse position)](http://alpha.editor.p5js.org/AndreasRef/sketches/HyiUvrjEf)

<iframe src="https://alpha.editor.p5js.org/embed/HyiUvrjEf" width="400px" height="400px"></iframe>

#### *Exercise: Control some aspect of your face from the first exercise with mouse, keyboard, noise, random, sin and/or map*

[example](https://editor.p5js.org/guillaume_slizewicz/sketches/Ba235DNF5)

<iframe src="https://alpha.editor.p5js.org/embed/Ba235DNF5" width="400px" height="400px"></iframe>



### Machine Learning

## ml5js templates, examples and exercises



### 1) Train a classification algorithm

[Simple template: Webcam classifier ](https://editor.p5js.org/AndreasRef/sketches/BJkaHBMYm)

[Webcam classifier + image](https://editor.p5js.org/AndreasRef/sketches/rJqk5_1aX)

[Webcam classifier + sound](https://editor.p5js.org/AndreasRef/sketches/ryLlIOJpX)

[Webcam classifier + filter](https://editor.p5js.org/AndreasRef/sketches/B19EA7x6Q)

[Advanced example: Trainable Camera](https://editor.p5js.org/AndreasRef/sketches/BynhuHsqX)

*Exercise: Train a classifier to distinguish between two  different classes. Make something happen (visuals or sound) for one or  both the classes.*



### 2) MobileNet pretrained classification

[Simple template: Mobilenet pretrained classification](https://editor.p5js.org/AndreasRef/sketches/H1L-KrzFQ)

[ImageNet list of classes](https://github.com/ml5js/ml5-library/blob/master/src/utils/IMAGENET_CLASSES.js)

[Advanced example: Classification to speech](https://editor.p5js.org/AndreasRef/sketches/Sk8M6mqh7)

*Exercise: Pick an object (or a few objects) for the model to recognise. Make something happen when your object(s) gets detected.*



### 3) Train a regression algorithm

[Simple template: Webcam regressor](https://editor.p5js.org/AndreasRef/sketches/B1g7ds0wm)

[Advanced example: Playback rate camera](https://editor.p5js.org/AndreasRef/sketches/HyEDToYnQ)

*Exercise: Train the regressor and use the continuous output value to control something.*



### 4) PoseNet pose detection

[Simple template: Webcam PoseNet ](https://editor.p5js.org/AndreasRef/sketches/rkz42BzYQ)

[Advanced example: Draw with your nose](https://editor.p5js.org/AndreasRef/sketches/r1_w73FhQ)

*Exercise: Get the values of one or more bodyparts and use them to control something.*



### Other ml5js examples

[YOLO object detection](https://editor.p5js.org/AndreasRef/sketches/BkzwgLGt7)

[Webcam classifier w. 4 classes](https://editor.p5js.org/AndreasRef/sketches/H1bmq3Y27)

[Webcam classifier with loaded images](https://editor.p5js.org/AndreasRef/sketches/rkKG-QIim)

[word2vec (and other examples) on ml5js.org](https://ml5js.org/docs/word2vec-example)



### Other Face detection examples

[Bonne année](https://editor.p5js.org/guillaume_slizewicz/sketches/ZqvMLcts3)

[Face drawing +speech detection](https://editor.p5js.org/guillaume_slizewicz/sketches/Byvcj33zE)

[emotion face detection](https://editor.p5js.org/guillaume_slizewicz/sketches/HJaL7i3zV) from [Spencer Lee James](https://spencerleejames.com/project/emojicv)



### Links:

 [Martin Pirson instagram](https://www.instagram.com/mrtnprsn/)

[Guillaume Slizewicz twitter](https://twitter.com/Guillaume_Slize)

[Guillaume Slizewicz instagram](https://www.instagram.com/guillaume_slizewicz/)

