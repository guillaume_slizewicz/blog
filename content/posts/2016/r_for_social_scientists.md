---
title: "R for social scientists"
date: 2016-11-22T18:42:48+01:00
categories: [code]
tags: [university, data mining, data scrapping, analysis, design, data-visualisation]
language: en
slug: R_for_social_science
---

In August 2016, I joined a data science workshop organised in Copenhagen University, mainly aimed at Social Scientists.

During 4 weeks we learned how to mine, clean, analyse and visualise data. We delivered an analysis on Football players transfer that you can find [here](/pdf/R.pdf). 

![data](/img/data.jpeg)
